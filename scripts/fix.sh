#!/bin/bash

sed -i '9a Environment="KUBELET_EXTRA_ARGS=--node-ip=10.0.0.11"' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

sudo systemctl daemon-reload

sudo systemctl restart kubelet

echo $?