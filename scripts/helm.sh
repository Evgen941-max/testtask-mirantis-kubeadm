#! /bin/bash

# Install helm

curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sleep 1

sudo apt-get install apt-transport-https --yes
sleep 1

sudo snap install helm --classic

sudo apt-get update
sleep 1