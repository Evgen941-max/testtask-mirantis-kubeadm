1. You need use this command:
    helm repo add jenkins https://charts.jenkins.io
    helm repo update

2. Deploying a simple Jenkins instance:
    helm upgrade --install myjenkins jenkins/jenkins
    The output looks something like this:

    '''
    Release "myjenkins" does not exist. Installing it now.
    NAME: myjenkins
    LAST DEPLOYED: Tue Oct 19 08:13:11 2021
    NAMESPACE: default
    STATUS: deployed
    REVISION: 1
    NOTES:
    1. Get your 'admin' user password by running:
    kubectl exec --namespace default -it svc/myjenkins -c jenkins -- /bin/cat /run/secrets/chart-admin-password && echo
    2. Get the Jenkins URL to visit by running these commands in the same shell:
    echo http://127.0.0.1:8080
    kubectl --namespace default port-forward svc/myjenkins 8080:8080

    3. Login with the password from step 1 and the username: admin
    4. Configure security realm and authorization strategy
    5. Use Jenkins Configuration as Code by specifying configScripts in your values.yaml file, see documentation: http:///configuration-as-code and examples: https://github.com/jenkinsci/configuration-as-code-plugin/tree/master/demos

    For more information on running Jenkins on Kubernetes, visit:
    https://cloud.google.com/solutions/jenkins-on-container-engine

    For more information about Jenkins Configuration as Code, visit:
    https://jenkins.io/projects/jcasc/


    NOTE: Consider using a custom image with pre-installed plugins
    '''
4. You need wait, when this pod been created. You can check it: 
    kubectl get pods --namespace jenkins

5. After that, you need use commad, that find Jenkins password:
    kubectl exec --namespace default -it svc/myjenkins -c jenkins -- /bin/cat /run/secrets/chart-admin-password && echo
    User Jenkins: admin

6. Use can use this command, that forward port Jenkins:
    kubectl --namespace default port-forward svc/myjenkins 8080:8080
    Output:
        Forwarding from 127.0.0.1:8080 -> 8080
        Forwarding from [::1]:8080 -> 8080

7. Now you need to configure the local Kubernetes connection to Jenkins
    You need install plugin for Kubernetis in your Jenkins.
    After that, you need in your pc change directory:

    cd ~/.kube/

    In this directory is file 'config'. Open this file in your code editor:
        1. You need find line: 'certificate-authority: /home/$(whoami)/.minikube/ca.crt'
        Use this command: cat /home/$(whoami)/.minikube/ca.crt | base64 -w 0; echo
        Change the line from 'certificate-authority:' to this 'certificate-authority-data:' and paste the result of the above command here.

        2. You need find line: 'client-certificate: /home/$(whoami)/.minikube/profiles/minikube/client.crt'
        Use this command: cat /home/$(whoami)/.minikube/profiles/minikube/client.crt | base64 -w 0; echo
        Change the line from 'client-certificate:' to this 'client-certificate-data:' and paste the result of the above command here.

        3. You need find line: 'client-key: /home/$(whoami)/.minikube/profiles/minikube/client.key'
        Use this command: cat /home/$(whoami)/.minikube/profiles/minikube/client.key | base64 -w 0; echo
        Change the line from 'certificate-key:' to this 'certificate-key-data:' and paste the result of the above command here.

    After that, you can try connect you Jenkins with local Kubernetes

8. You can create new job. You need to choose 'pipeline' and paste this code. But you need create secret:
    Use this command:
    'kubectl create secret docker-registry docker-credentials --docker-username=[userid] --docker-password=[Docker Hub access token] --docker-email=[user email address] --namespace jenkins'

    Code for pipeline:

    pipeline {
    agent {
        kubernetes {
        yaml """
    kind: Pod
    spec:
    containers:
    - name: kaniko
        image: gcr.io/kaniko-project/executor:debug
        imagePullPolicy: Always
        command:
        - sleep
        args:
        - 9999999
        volumeMounts:
        - name: jenkins-docker-cfg
            mountPath: /kaniko/.docker
    volumes:
    - name: jenkins-docker-cfg
        projected:
        sources:
        - secret:
            name: docker-credentials
            items:
                - key: .dockerconfigjson
                path: config.json
    """
        }
    }
    stages {
        stage('Build with Kaniko') {
        steps {
            container(name: 'kaniko', shell: '/busybox/sh') {
            sh '''#!/busybox/sh
                echo "FROM jenkins/inbound-agent:latest" > Dockerfile
                /kaniko/executor --context `pwd` --destination (Your docker-registry name)/hello-kaniko:latest
            '''
            }
        }
        }
    }
    }