
# Vagrantfile and Scripts to Automate Kubernetes

# Documentation

Current k8s version for CKA, CKAD and CKS exam: 1.22.

Refer this link for documentation: https://devopscube.com/kubernetes-cluster-vagrant/

## 🚀 CKA, CKAD, CKS or KCNA Voucher Codes

If you are preparing for CKA, CKAD, CKS or KCNA exam, **get 22 discount%** today using code **DCUBE22** at https://kube.promo/latest. It is a limited time offer

So that the host only networks can be in any range, not just 192.168.56.0/21 as described here:
https://discuss.hashicorp.com/t/vagrant-2-2-18-osx-11-6-cannot-create-private-network/30984/23


# There are two options for deploying Jenkins: 

    THIS CONFIGURE USE KUBEADM FOR DEPLOY JENKINS ON KUBERNETES
    1. Use yaml file, you can read instruction in this directory - its file 'Use YAML'
    2. Use helm chart, you can read instruction in this directory - its file 'Use chart'

# https://devopscube.com/setup-kubernetes-cluster-kubeadm/